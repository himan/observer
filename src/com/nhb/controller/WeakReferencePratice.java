package com.nhb.controller;

import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;

public class WeakReferencePratice {

	public static void main(String[] args) throws FileNotFoundException {
		Data data = new Data();
	    WeakReference<Data> new1 = new WeakReference<Data>(data);
	    Data new2 = data;
	    new2 = null;
	    data = null;
	    System.gc();//告诉垃圾收集器打算进行垃圾收集，而垃圾收集器进不进行收集是不确定的 ,所以下面延迟2秒
	    try {
	      Thread.sleep(2000);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
	    System.out.println(new1.get().num);

	}

}

class Data{
	int num = 1;
}
