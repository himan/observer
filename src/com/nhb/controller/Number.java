package com.nhb.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @Description: TODO
 * @author liulei
 * @date 2018年4月11日 上午8:52:53
 */
public class Number {

	public static void main(String[] args) {
		// 1 创建学生编号集合
		List<Integer> kids = new ArrayList<>();
		for (int i = 1; i <= 100; i++) {
			kids.add(i);
		}
		//2 返回的数据顺序集合
		List<Integer> sorts = new ArrayList<>();
		
		//3 计数常量 用于喊数 
		int num = 0;
		
		//4 判断是否还有在喊数的学生
		while(kids.size()>0) {
			Iterator<Integer> iterator = kids.iterator();
			// 5 遍历学生集合
			while(iterator.hasNext()) {
				Integer kid = iterator.next();
				num++;
				// 6 获取喊到9的学生记录,并从圈中剔除
				if(num%9==0) {
					sorts.add(kid);
					iterator.remove();
				}
			}
		}
		//7 输出报数顺序
		System.out.println(sorts);
	}
}

