package com.designmode.service;

/**
 * @Description: 观察者
 * @author liulei
 * @date 2018年4月16日 上午10:49:17
 */
public interface Observer {
	/**
	 * @Description: 更新观察者数据
	 * @param subject:主题
	 * @param obj:推送消息
	 */
	void update(Subject subject,Object obj);
}

