package com.designmode.service;

/**
 * @Description: 主题
 * @author liulei
 * @date 2018年4月16日 上午10:48:54
 */
public interface Subject {
	void registerObserver(Observer observer);
	void removeObserver(Observer observer);
	// 激活观察者状态更新
	void notifyObservers();
}

