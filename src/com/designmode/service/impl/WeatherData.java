package com.designmode.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.designmode.service.Observer;
import com.designmode.service.Subject;

/**
 * @Description: 天气数据
 * @author liulei
 * @date 2018年4月16日 上午10:50:07
 */
public class WeatherData implements Subject {
	private List<Observer> observers;
	private float temp;
	private float humidity;
	private float pressure;
	private boolean changed;// 更新标识

	public WeatherData() {
		// 实例初始化再创建对象,缩短生命周期,减少资源占用
		observers = new ArrayList<>();
	}

	@Override
	public void registerObserver(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}

	public void notifyObservers(Object org) {
		if (changed) {
			for (Observer observer : observers) {
				observer.update(this, org);
			}
			changed = false;
		}
	}

	@Override
	public void notifyObservers() {
		notifyObservers(null);
	}

	// 自身的更新方法
	public void measurementsChanged() {
		setChanged();
		notifyObservers();
	}

	public void setMeasurements(float temp, float humidity, float pressure) {
		this.temp = temp;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}

	// 设置更新标识,控制更新频率和状态
	private void setChanged() {
		changed = true;
	}

	public float getTemp() {
		return temp;
	}

	public float getHumidity() {
		return humidity;
	}

	public float getPressure() {
		return pressure;
	}
	
}
