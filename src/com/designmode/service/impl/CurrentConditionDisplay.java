package com.designmode.service.impl;

import com.designmode.service.DisplayElement;
import com.designmode.service.Observer;
import com.designmode.service.Subject;

/**
 * @Description: 显示当前观测值
 * @author liulei
 * @date 2018年4月16日 上午10:50:42
 */
public class CurrentConditionDisplay implements Observer,DisplayElement{
	private float temp;
	private float humidity;
	private Subject subject;

	// 接收主题的消息激活
	public CurrentConditionDisplay(Subject subject) {
		this.subject=subject;
		subject.registerObserver(this);
	}

	/**
	 * @Description: 更新信息
	 * @param subject:消息源主题
	 * @param obj:接收参数
	 */
	@Override
	public void update(Subject subject,Object obj) {
		if(subject instanceof WeatherData) {
			WeatherData WeatherData = (WeatherData)subject;
			this.temp=WeatherData.getTemp();
			this.humidity=WeatherData.getHumidity();
			display();
		}
	}
	@Override
	public void display() {
		System.out.println("Current conditions: "+temp+" F degrees and "+humidity+"% humidity");
	}

	public void cancelRegister() {
		subject.removeObserver(this);
	}
}

