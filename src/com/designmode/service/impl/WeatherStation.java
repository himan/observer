package com.designmode.service.impl;

import com.designmode.service.Observer;
import com.designmode.service.Subject;

/**
 * @Description: 天气情况
 * @author liulei
 * @date 2018年4月16日 上午11:20:03
 */
public class WeatherStation {

	public static void main(String[] args) {
		String property = System.getProperty("os.name");
		System.out.println(property);
		WeatherData weatherData = new WeatherData();
		Observer currentDisplay = new CurrentConditionDisplay(weatherData);
		weatherData.setMeasurements(23, 65, 30.4f);
	}

}

