package com.designmode.service;

/**
 * @Description: 显示元素
 * @author liulei
 * @date 2018年4月16日 上午10:49:34
 */
public interface DisplayElement {
	void display();
}

